package org.artur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderKafkaProducerApiAplication {
    public static void main(String[] args){
        SpringApplication.run(OrderKafkaProducerApiAplication.class, args);
    }
}