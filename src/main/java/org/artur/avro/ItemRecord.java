/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package org.artur.avro;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class ItemRecord extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -4916803163919574123L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"ItemRecord\",\"namespace\":\"org.artur.avro\",\"fields\":[{\"name\":\"idProduto\",\"type\":\"long\"},{\"name\":\"descricao\",\"type\":\"string\"},{\"name\":\"qtd\",\"type\":\"int\"},{\"name\":\"preco\",\"type\":\"double\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<ItemRecord> ENCODER =
      new BinaryMessageEncoder<ItemRecord>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<ItemRecord> DECODER =
      new BinaryMessageDecoder<ItemRecord>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<ItemRecord> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<ItemRecord> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<ItemRecord> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<ItemRecord>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this ItemRecord to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a ItemRecord from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a ItemRecord instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static ItemRecord fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private long idProduto;
   private java.lang.CharSequence descricao;
   private int qtd;
   private double preco;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public ItemRecord() {}

  /**
   * All-args constructor.
   * @param idProduto The new value for idProduto
   * @param descricao The new value for descricao
   * @param qtd The new value for qtd
   * @param preco The new value for preco
   */
  public ItemRecord(java.lang.Long idProduto, java.lang.CharSequence descricao, java.lang.Integer qtd, java.lang.Double preco) {
    this.idProduto = idProduto;
    this.descricao = descricao;
    this.qtd = qtd;
    this.preco = preco;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return idProduto;
    case 1: return descricao;
    case 2: return qtd;
    case 3: return preco;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: idProduto = (java.lang.Long)value$; break;
    case 1: descricao = (java.lang.CharSequence)value$; break;
    case 2: qtd = (java.lang.Integer)value$; break;
    case 3: preco = (java.lang.Double)value$; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'idProduto' field.
   * @return The value of the 'idProduto' field.
   */
  public long getIdProduto() {
    return idProduto;
  }


  /**
   * Sets the value of the 'idProduto' field.
   * @param value the value to set.
   */
  public void setIdProduto(long value) {
    this.idProduto = value;
  }

  /**
   * Gets the value of the 'descricao' field.
   * @return The value of the 'descricao' field.
   */
  public java.lang.CharSequence getDescricao() {
    return descricao;
  }


  /**
   * Sets the value of the 'descricao' field.
   * @param value the value to set.
   */
  public void setDescricao(java.lang.CharSequence value) {
    this.descricao = value;
  }

  /**
   * Gets the value of the 'qtd' field.
   * @return The value of the 'qtd' field.
   */
  public int getQtd() {
    return qtd;
  }


  /**
   * Sets the value of the 'qtd' field.
   * @param value the value to set.
   */
  public void setQtd(int value) {
    this.qtd = value;
  }

  /**
   * Gets the value of the 'preco' field.
   * @return The value of the 'preco' field.
   */
  public double getPreco() {
    return preco;
  }


  /**
   * Sets the value of the 'preco' field.
   * @param value the value to set.
   */
  public void setPreco(double value) {
    this.preco = value;
  }

  /**
   * Creates a new ItemRecord RecordBuilder.
   * @return A new ItemRecord RecordBuilder
   */
  public static org.artur.avro.ItemRecord.Builder newBuilder() {
    return new org.artur.avro.ItemRecord.Builder();
  }

  /**
   * Creates a new ItemRecord RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new ItemRecord RecordBuilder
   */
  public static org.artur.avro.ItemRecord.Builder newBuilder(org.artur.avro.ItemRecord.Builder other) {
    if (other == null) {
      return new org.artur.avro.ItemRecord.Builder();
    } else {
      return new org.artur.avro.ItemRecord.Builder(other);
    }
  }

  /**
   * Creates a new ItemRecord RecordBuilder by copying an existing ItemRecord instance.
   * @param other The existing instance to copy.
   * @return A new ItemRecord RecordBuilder
   */
  public static org.artur.avro.ItemRecord.Builder newBuilder(org.artur.avro.ItemRecord other) {
    if (other == null) {
      return new org.artur.avro.ItemRecord.Builder();
    } else {
      return new org.artur.avro.ItemRecord.Builder(other);
    }
  }

  /**
   * RecordBuilder for ItemRecord instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<ItemRecord>
    implements org.apache.avro.data.RecordBuilder<ItemRecord> {

    private long idProduto;
    private java.lang.CharSequence descricao;
    private int qtd;
    private double preco;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(org.artur.avro.ItemRecord.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.idProduto)) {
        this.idProduto = data().deepCopy(fields()[0].schema(), other.idProduto);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.descricao)) {
        this.descricao = data().deepCopy(fields()[1].schema(), other.descricao);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.qtd)) {
        this.qtd = data().deepCopy(fields()[2].schema(), other.qtd);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
      if (isValidValue(fields()[3], other.preco)) {
        this.preco = data().deepCopy(fields()[3].schema(), other.preco);
        fieldSetFlags()[3] = other.fieldSetFlags()[3];
      }
    }

    /**
     * Creates a Builder by copying an existing ItemRecord instance
     * @param other The existing instance to copy.
     */
    private Builder(org.artur.avro.ItemRecord other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.idProduto)) {
        this.idProduto = data().deepCopy(fields()[0].schema(), other.idProduto);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.descricao)) {
        this.descricao = data().deepCopy(fields()[1].schema(), other.descricao);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.qtd)) {
        this.qtd = data().deepCopy(fields()[2].schema(), other.qtd);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.preco)) {
        this.preco = data().deepCopy(fields()[3].schema(), other.preco);
        fieldSetFlags()[3] = true;
      }
    }

    /**
      * Gets the value of the 'idProduto' field.
      * @return The value.
      */
    public long getIdProduto() {
      return idProduto;
    }


    /**
      * Sets the value of the 'idProduto' field.
      * @param value The value of 'idProduto'.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder setIdProduto(long value) {
      validate(fields()[0], value);
      this.idProduto = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'idProduto' field has been set.
      * @return True if the 'idProduto' field has been set, false otherwise.
      */
    public boolean hasIdProduto() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'idProduto' field.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder clearIdProduto() {
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'descricao' field.
      * @return The value.
      */
    public java.lang.CharSequence getDescricao() {
      return descricao;
    }


    /**
      * Sets the value of the 'descricao' field.
      * @param value The value of 'descricao'.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder setDescricao(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.descricao = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'descricao' field has been set.
      * @return True if the 'descricao' field has been set, false otherwise.
      */
    public boolean hasDescricao() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'descricao' field.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder clearDescricao() {
      descricao = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'qtd' field.
      * @return The value.
      */
    public int getQtd() {
      return qtd;
    }


    /**
      * Sets the value of the 'qtd' field.
      * @param value The value of 'qtd'.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder setQtd(int value) {
      validate(fields()[2], value);
      this.qtd = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'qtd' field has been set.
      * @return True if the 'qtd' field has been set, false otherwise.
      */
    public boolean hasQtd() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'qtd' field.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder clearQtd() {
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'preco' field.
      * @return The value.
      */
    public double getPreco() {
      return preco;
    }


    /**
      * Sets the value of the 'preco' field.
      * @param value The value of 'preco'.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder setPreco(double value) {
      validate(fields()[3], value);
      this.preco = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'preco' field has been set.
      * @return True if the 'preco' field has been set, false otherwise.
      */
    public boolean hasPreco() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'preco' field.
      * @return This builder.
      */
    public org.artur.avro.ItemRecord.Builder clearPreco() {
      fieldSetFlags()[3] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ItemRecord build() {
      try {
        ItemRecord record = new ItemRecord();
        record.idProduto = fieldSetFlags()[0] ? this.idProduto : (java.lang.Long) defaultValue(fields()[0]);
        record.descricao = fieldSetFlags()[1] ? this.descricao : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.qtd = fieldSetFlags()[2] ? this.qtd : (java.lang.Integer) defaultValue(fields()[2]);
        record.preco = fieldSetFlags()[3] ? this.preco : (java.lang.Double) defaultValue(fields()[3]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<ItemRecord>
    WRITER$ = (org.apache.avro.io.DatumWriter<ItemRecord>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<ItemRecord>
    READER$ = (org.apache.avro.io.DatumReader<ItemRecord>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeLong(this.idProduto);

    out.writeString(this.descricao);

    out.writeInt(this.qtd);

    out.writeDouble(this.preco);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.idProduto = in.readLong();

      this.descricao = in.readString(this.descricao instanceof Utf8 ? (Utf8)this.descricao : null);

      this.qtd = in.readInt();

      this.preco = in.readDouble();

    } else {
      for (int i = 0; i < 4; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.idProduto = in.readLong();
          break;

        case 1:
          this.descricao = in.readString(this.descricao instanceof Utf8 ? (Utf8)this.descricao : null);
          break;

        case 2:
          this.qtd = in.readInt();
          break;

        case 3:
          this.preco = in.readDouble();
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










