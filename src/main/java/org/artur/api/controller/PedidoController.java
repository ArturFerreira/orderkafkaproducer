package org.artur.api.controller;

import lombok.RequiredArgsConstructor;
import org.artur.domain.dto.PedidoItemDto;
import org.artur.domain.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/pedido")
@RequiredArgsConstructor
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PedidoItemDto salvar(@RequestBody PedidoItemDto pedidoItemDto){
        pedidoItemDto.setIdentificador(UUID.randomUUID().toString());

        return pedidoService.salvar(pedidoItemDto);
    }
}
