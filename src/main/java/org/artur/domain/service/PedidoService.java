package org.artur.domain.service;

import lombok.RequiredArgsConstructor;
import org.artur.avro.ItemRecord;
import org.artur.avro.PedidoRecord;
import org.artur.domain.dto.ItemDto;
import org.artur.domain.dto.PedidoItemDto;
import org.artur.domain.enumeration.StatusErro;
import org.artur.domain.enumeration.StatusEspera;
import org.artur.domain.enumeration.StatusProcessamento;
import org.artur.domain.enumeration.StatusTracking;
import org.artur.domain.model.Item;
import org.artur.domain.model.Pedido;
import org.artur.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private KafkaTemplate<String, PedidoRecord> kafkaTemplate;

    @Transactional
    public PedidoItemDto salvar(PedidoItemDto pedidoItemDto){
        Pedido pedido = new Pedido();

        pedido.setIdentificador(pedidoItemDto.getIdentificador());
        pedido.setDt(LocalDateTime.now());
        pedido.setDtProcessamento(LocalDateTime.parse(pedidoItemDto.getDtProcessamento()));
        pedido.setCanalVenda(pedidoItemDto.getCanalVenda());
        pedido.setTotalPedido(pedidoItemDto.getTotalPedido());
        pedido.setTotalDesconto(pedidoItemDto.getTotalDesconto());
        pedido.setTotalItens(pedidoItemDto.getTotalItens());
        pedido.setClienteId(pedidoItemDto.getIdCliente());
        pedido.setFilialId(pedidoItemDto.getIdFilial());
        pedido.setStatusProcessamento(StatusProcessamento.valueOf(pedidoItemDto.getStatusProcessamento()));
        pedido.setStatusErro(StatusErro.valueOf(pedidoItemDto.getStatusErro()));
        pedido.setStatusTracking(StatusTracking.valueOf(pedidoItemDto.getStatusTracking()));
        pedido.setStatusEspera(StatusEspera.valueOf(pedidoItemDto.getStatusEspera()));

        List<ItemDto> itensDto = pedidoItemDto.getItens();
        List<Item> itens = itensDto.stream()
                .map(itemDTO -> {
                    Item item = new Item();
                    item.setDt(LocalDateTime.now());
                    item.setProdutoId((long) itemDTO.getIdProduto());
                    item.setDescricao(itemDTO.getDescricao());
                    item.setQtd(itemDTO.getQtd());
                    item.setPreco(itemDTO.getPreco());
                    item.setPedido(pedido);
                    return item;
                })
                .collect(Collectors.toList());

        pedido.setItens(itens);

        pedidoRepository.save(pedido);

        kafkaTemplate.send("pedido-topic", pedidoItemDto.getIdentificador(), preencherAvroPedido(pedidoItemDto));
        return pedidoItemDto;
    }

    public PedidoRecord preencherAvroPedido(PedidoItemDto pedidoItemDto){
        List<ItemRecord> itemRecordList = pedidoItemDto.getItens().stream()
                .map(itemDTO -> {
                    ItemRecord itemRecord = ItemRecord.newBuilder()
                            .setDescricao(itemDTO.getDescricao())
                            .setPreco(itemDTO.getPreco())
                            .setIdProduto(itemDTO.getIdProduto())
                            .setQtd(itemDTO.getQtd())
                            .build();
                    return itemRecord;
                })
                .collect(Collectors.toList());

        PedidoRecord pedidoRecord = PedidoRecord.newBuilder()
                .setIdentificador(pedidoItemDto.getIdentificador())
                .setDtProcessamento(pedidoItemDto.getDtProcessamento())
                .setCanalVenda(pedidoItemDto.getCanalVenda())
                .setTotalPedido(pedidoItemDto.getTotalPedido())
                .setTotalDesconto(pedidoItemDto.getTotalDesconto())
                .setTotalItens(pedidoItemDto.getTotalItens())
                .setIdCliente(pedidoItemDto.getIdCliente())
                .setIdFilial(pedidoItemDto.getIdFilial())
                .setStatusProcessamento(pedidoItemDto.getStatusProcessamento())
                .setStatusErro(pedidoItemDto.getStatusErro())
                .setStatusTracking(pedidoItemDto.getStatusTracking())
                .setStatusEspera(pedidoItemDto.getStatusEspera())
                .setItens(itemRecordList)
                .build();

        return pedidoRecord;
    }

}
