package org.artur.domain.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime dt;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Pedido pedido;

    private Long produtoId;

    private String descricao;

    private Integer qtd;

    private Double preco;
}
