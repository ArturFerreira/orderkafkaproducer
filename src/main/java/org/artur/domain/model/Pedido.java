package org.artur.domain.model;

import jakarta.persistence.*;
import lombok.Data;
import org.artur.domain.enumeration.StatusErro;
import org.artur.domain.enumeration.StatusEspera;
import org.artur.domain.enumeration.StatusProcessamento;
import org.artur.domain.enumeration.StatusTracking;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Pedido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String identificador;

    private LocalDateTime dt;

    private LocalDateTime dtProcessamento;

    private Integer canalVenda;

    private Double totalPedido;

    private Double totalDesconto;

    private Integer totalItens;

    private Long clienteId;

    private Long filialId;

    @Enumerated(EnumType.STRING)
    private StatusProcessamento statusProcessamento;

    @Enumerated(EnumType.STRING)
    private StatusErro statusErro;

    @Enumerated(EnumType.STRING)
    private StatusTracking statusTracking;

    @Enumerated(EnumType.STRING)
    private StatusEspera statusEspera;

    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, orphanRemoval = false)
    private List<Item> itens = new ArrayList<>();

}
