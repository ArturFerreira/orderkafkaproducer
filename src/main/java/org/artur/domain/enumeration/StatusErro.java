package org.artur.domain.enumeration;

public enum StatusErro {
    SEM_ERRO("SEM_ERRO", 1),
    ERRO_CONFIGURACAO("ERRO_CONFIGURACAO", 2),
    ERRO_SEM_ITENS("ERRO_SEM_ITENS", 3),
    ERRO_PROCESSAMENTO("ERRO_PROCESSAMENTO", 4),
    ERRO_CLIENTE("ERRO_CLIENTE", 5),
    ERRO_PAGAMENTO("ERRO_PAGAMENTO", 6),
    ERRO_CANCELAMENTO_PAGAMENTO("ERRO_CANCELAMENTO_PAGAMENTO", 7);

    private final String chave;
    private final int valor;

    StatusErro(String chave, int valor){
        this.valor = valor;
        this.chave = chave;
    }

    public String getChave(){
        return chave;
    }

    public int getValor(){
        return valor;
    }
}
