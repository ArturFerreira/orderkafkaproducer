package org.artur.domain.enumeration;

public enum StatusTracking {
    NMP("NAO_MAPEADO", 1),
    MAP("MAPEADO", 2),
    WMS("EM_SEPARACAO", 3),
    NFS("NOTA_FISCAL_DISPONIVEL", 4),
    FAT("FATURADO", 5),
    ETR("ENTREGUE_TRANSPORTADOR", 6),
    ENT("ENTREGA_CONCLUIDA", 7),
    MNA("LIMITE_CREDITO_REPROVADA", 8),
    CAN("CANCELADO", 9),
    PAD("ENTREGUE_PAGO", 10),
    PDR("PEDIDO_RECUSADO", 11);

    private final String chave;
    private final int valor;

    StatusTracking(String chave, int valor){
        this.valor = valor;
        this.chave = chave;
    }

    public String getChave(){
        return chave;
    }

    public int getValor(){
        return valor;
    }
}
