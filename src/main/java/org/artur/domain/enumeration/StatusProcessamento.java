package org.artur.domain.enumeration;

public enum StatusProcessamento {
    NAO_PROCESSADO("NAO_PROCESSADO", 1),
    PROCESSANDO("PROCESSANDO", 2),
    FALHA_CANCELADO("FALHA_CANCELADO", 3),
    FALHA_ANALISAR("FALHA_ANALISAR", 4),
    PROCESSADO("PROCESSADO", 5),
    LIBERACAO_PENDENTE("LIBERACAO_PENDENTE", 6);

    private final String chave;
    private final int valor;

    StatusProcessamento(String chave, int valor){
        this.valor = valor;
        this.chave = chave;
    }

    public String getChave(){
        return chave;
    }

    public int getValor(){
        return valor;
    }
}
