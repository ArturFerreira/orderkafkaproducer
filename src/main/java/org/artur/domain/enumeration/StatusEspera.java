package org.artur.domain.enumeration;

public enum StatusEspera {
    SEM_ESPERA("SEM_ESPERA", 1),
    AGUARDANDO_PAGAMENTO("AGUARDANDO_PAGAMENTO", 2),
    PEDIDO_EM_DIGITACAO("PEDIDO_EM_DIGITACAO", 3),
    PEDIDO_CANCELADO("PEDIDO_CANCELADO", 4),
    LIBERACAO_NEGADA("LIBERACAO_NEGADA", 5),
    PEDIDO_STATUS_INVALIDO("PEDIDO_STATUS_INVALIDO", 6),
    PEDIDO_CANCELAMENTO_PAGAMENTO("PEDIDO_CANCELAMENTO_PAGAMENTO", 7);

    private final String chave;
    private final int valor;

    StatusEspera(String chave, int valor){
        this.valor = valor;
        this.chave = chave;
    }

    public String getChave(){
        return chave;
    }

    public int getValor(){
        return valor;
    }
}
