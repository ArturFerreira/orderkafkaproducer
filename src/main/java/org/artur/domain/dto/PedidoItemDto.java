package org.artur.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.artur.domain.enumeration.StatusErro;
import org.artur.domain.enumeration.StatusEspera;
import org.artur.domain.enumeration.StatusProcessamento;
import org.artur.domain.enumeration.StatusTracking;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PedidoItemDto {

    private String identificador;

    private String dtProcessamento;

    private int canalVenda;

    private double totalPedido;

    private double totalDesconto;

    private int totalItens;

    private long idCliente;

    private long idFilial;

    private String statusProcessamento;

    private String statusErro;

    private String statusTracking;

    private String statusEspera;

    private List<ItemDto> itens = new ArrayList<>();
}
