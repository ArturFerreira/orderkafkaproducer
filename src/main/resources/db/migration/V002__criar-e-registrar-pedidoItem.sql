CREATE table pedidoItem(
  id bigint identity(1,1),
  dt datetime,
  idPedido bigint not null,
  idProduto bigint not null,
  descricao varchar(255) not null,
  totalPedido numeric(16,4) not null,
  qtd int not null,
  preco numeric(16,4) not null,
  primary key (id),
)